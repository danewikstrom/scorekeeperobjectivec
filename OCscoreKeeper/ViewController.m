//
//  ViewController.m
//  OCscoreKeeper
//
//  Created by Dane Wikstrom on 9/22/17.
//  Copyright © 2017 Dane Wikstrom. All rights reserved.
//

#import "ViewController.h"

@interface ViewController () <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UILabel *lbl1;
@property (weak, nonatomic) IBOutlet UIStepper *step1;
@property (weak, nonatomic) IBOutlet UILabel *lbl2;
@property (weak, nonatomic) IBOutlet UIStepper *step2;
@property (weak, nonatomic) IBOutlet UITextField *textField1;
@property (weak, nonatomic) IBOutlet UITextField *textField2;
@property (weak, nonatomic) IBOutlet UIButton *reset;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    self.textField1.delegate = self;
    self.textField2.delegate = self;
}

- (IBAction)stepChange1:(UIStepper *)sender
{
    self.step1.value = sender.value;
    self.lbl1.text = [NSString stringWithFormat:@"%.0f", self.step1.value];
}

- (IBAction)sepChange2:(UIStepper *)sender
{
    self.step2.value = sender.value;
    self.lbl2.text = [NSString stringWithFormat:@"%.0f", self.step2.value];
}

- (IBAction)resetButton:(UIButton *)sender
{
    self.step1.value = 0;
    self.lbl1.text = @"0";
    self.step2.value = 0;
    self.lbl2.text = @"0";
}

//MARK: UITextFieldDelegate
//Dismisses keyboard when user presses Done on keyboard
- (bool)textFieldShouldReturn:(UITextField*)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
