//
//  main.m
//  OCscoreKeeper
//
//  Created by Dane Wikstrom on 9/22/17.
//  Copyright © 2017 Dane Wikstrom. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
